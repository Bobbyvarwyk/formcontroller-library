# FormController.js


## What does it do?
This GIT repository contains the FormController javascript library. FormController checks all the forms on a page real time for the correct syntax and minimum characters.
It can be used for text fields, email and password fields. The submit button is enabled when one of these input fields is correctly filled in. 

## What does it need?
The FormController needs a Jquery CDN or file to work, like this one :

https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js

Make sure you call the Jquery before you call the FormConroller file, which is called like this :
<script src="pathtofile/formController.js"> </script>

## What to change before installing FormController.js
There is one component you need to change, which is the name of your submit button. Which is done in the following way :

var submitButton = document.getElementById("YourButtonID");


## How to initiate
After succesfully linking the files (Jquery & FormController.js) you can simply initiate it by calling the following code :

<script type="text/javascript">
	var formCheck = new FormControls();
	formCheck.init();
</script>

After doing this, every form on the page will be checked by FormController.js

## How does it look 
The front-end of FormController looks like this (project example) :

![picture](img/formcontroller.js.png)


