<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>FormController.js</title>
<link rel="stylesheet" type="text/css" href="styles.css" />
<link href="https://fonts.googleapis.com/css?family=Heebo&display=swap" rel="stylesheet">
</head>

<body>
    <div class="container">
      <h1> Confirm your account </h1>
        <form action="/">
          <input type="text" name="username" placeholder="Username.." required>
          <input type="email" name="email" placeholder="Email.." required>
          <input type="password" name="password" placeholder="Password.." required>
          <button type="submit" id="submit-btn" value="Verzend formulier"> <span> Confirm Account </span> </button>
        </form>
    </div>
 

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="js/formController.js"> </script>
  <script type="text/javascript">
       var formCheck = new FormControls();
        formCheck.init();
  </script>
</body>
</html>