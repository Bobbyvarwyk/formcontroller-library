/*
    This file contains the custom formchecker that checks the input fields of your form for the right amount of characters and the right email template.
    simply call the .init to initialize this script on your page. 
*/
var FormControls = function () {

    var redColor = "#96281b";
    var greenColor = "#1e824c";
    // Amount of minimum characters for normal (text, name, etc) field
    var normalCharacters = 6;
    // Amount of minimum characters for password field
    var passwordCharacters = 8;

    var submitButton = document.getElementById("submit-btn");

    // Init the entire script
    function init() {
        var formElements = document.forms[0].elements;
        for (var i = 0; i <= formElements.length - 1; i++) {
            addBorder(formElements[i]);
        }

        $(document).ready(function(){
            $("input").keyup(function(){
               checkInputStatus(this);
            });
        });
    }

    function addBorder(selector) {
       if (['text', 'email', 'password'].indexOf(selector.getAttribute('type')) >= 0) {
            setBorder(selector, redColor);
            submitActive(false);
       } else {
           console.log("Couldn't add border to elements");
       }
    }

    // Check if the selected field is filled in properly
    function checkInputStatus(selector) {
        var string = selector.value;
        if (selector.getAttribute('type') == "text" && string.length >= normalCharacters) {
            setBorder(selector, greenColor);
            submitActive(true);
        } else if (selector.getAttribute('type') == "password" && string.length >= passwordCharacters) {
            setBorder(selector, greenColor);
            submitActive(true);
        } else if (validateMail(selector.value)) {
            setBorder(selector, greenColor);
            submitActive(true);
        } else {
            setBorder(selector, redColor);
            submitActive(false);
        }
    }

    function submitActive(status) {
        if (status == true) {
            submitButton.removeAttribute("disabled");
            submitButton.style.opacity = 1;
        } else {
            submitButton.setAttribute("disabled", true);
            submitButton.style.opacity = 0.2;
        }
    }

    // Validate the email for the correct syntax (Must contain @ and .com or other extension)
    function validateMail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    // Set the border bottom to the selected element
    function setBorder(selector, color) {
        selector.setAttribute("style", "border: 3px solid " + color);
    }

    return {
        init : init
    }
}